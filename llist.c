#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct llist
{
    double data;
    struct llist *next;
};

struct llist *llist_create(double value)
{
    struct llist *head = malloc(sizeof(*head));
    if (head) {
        head->data = value;
        head->next = NULL;
    }
    return head;
}

void llist_destroy(struct llist *list)
{
    while(list) {
        struct llist *tmp = list;
        list = list->next;
        free(tmp);
    }
}

//Adds to end of llist--------------------------------
/*void llist_add(struct llist *list, double value)*/
/*{*/
/*    struct llist *item = llist_create(value);*/
/*	while(list->next) {*/
/*		list = list->next;*/
/*	}*/

/*    list->next = item;*/
/*}*/

//Add to beginning
void llist_add(struct llist **list, double value)
{
	struct llist *new = llist_create(value);	

	if (new) {
		new->next = *list;
		*list = new;
	}
}


//----------------------------------------------------

void llist_remove(struct llist **list)
{
    struct llist *new_head = (*list)->next;
    free(*list);
    *list = new_head;
}

void llist_print(struct llist *list)
{
	while (list) {
		printf("> %f\n", list->data);
		list = list->next;
	}
}

//Exercise 1
double llist_sum(struct llist *list)
{
	double sum = 0;
	while (list) {
		sum += list->data;
		list = list->next;
	}
	return sum;
}

void ll_append(struct llist *a, struct llist *b)
{
	while (a->next) {
		a = a->next;
	}

	a->next = b;
}

/*
 * Returns 0:match, 1:a>b, -1:a<b
 */
int cmp(double a, double b)
{
	if (a < b) {
		return -1;
	} else if (a > b) {
		return 1;
	} else {
		return 0; //0 returns match
	} 
}

bool ll_equal_by(struct llist *a, struct llist *b, int (*cmp)(double, double))
{
	while (a) {
		if (!b) { //If lists don't have same lengths
			return false;
		} else if ((*cmp)(a->data, b->data) == 1) {
			return false;
		}
		a = a->next;
		b = b->next;
	}
	return true;
}

bool compare(double a, double b, int(*cmp)(double, double))
{
	if ((*cmp)(a, b) == 1) {
		return false;
	} else {
		return true;
	}
}

void ll_flatten(struct llist *list, int(*cmp)(double, double))
{
	struct llist *begin_node = list, *next_node;

	while (begin_node != NULL && begin_node->next != NULL) {
		next_node = begin_node;
		while (next_node->next) {
			if ((*cmp)(begin_node->data, next_node->next->data) == 0) {
				struct llist *temp = next_node->next;
				next_node->next = next_node->next->next;
				free(temp);
			} else {
				next_node = next_node->next;
			}
		}
	begin_node = begin_node->next;
	}
}

bool ll_is_sorted(struct llist *list, int(*cmp)(double, double))
{
	struct llist *begin_node = list, *next_node;
	int count = 0, ascend = 0, descend = 0;

	while (begin_node != NULL && begin_node->next != NULL) {
		next_node = begin_node;
		while (next_node->next) {
			if ((*cmp)(begin_node->data, next_node->next->data) == -1) {
				ascend += 1;
				next_node = next_node->next;
			} else if ((*cmp)(begin_node->data, next_node->next->data) == 1){
				descend += 1;
				next_node = next_node->next;
			}
			count += 1;
		}
	begin_node = begin_node->next;
	}

	if (count == ascend || count == descend) {
		return true;
	} else {
		return false;
	}
}

void ll_reverse(struct llist **list)
{
	struct llist *cur_node = *list;
	struct llist *prev_node = NULL;
	struct llist *next_node;

	while (cur_node){
		next_node = cur_node->next;
		cur_node->next = prev_node;
		prev_node = cur_node;
		cur_node = next_node;
	}
	*list = prev_node;
}

bool ll_is_circular(struct llist *list)
{
	struct llist *begin = list;

/*	struct llist *test = list;*/
/*	while(test->next){*/
/*		test = test->next;*/
/*	}*/
/*		test->next = begin;*/

	while (list) {
		printf(" >%f\n", list->data);
		if (list->next == begin) {
			return true;
		}
		list = list->next;
	}
	return false;
}

void ll_insert_sorted(struct llist **list, double value)
{
	struct llist *new_node = llist_create(value);
	struct llist *prev_node = NULL;
	struct llist *cur_node = *list;
	while (cur_node && cur_node->data <= value) {
		prev_node = cur_node;
		cur_node = cur_node->next;
	}

	if (prev_node) {
		prev_node->next = new_node;
	} else {
		*list = new_node;
	}

	new_node->next = cur_node;
}



















int main(void)
{
	struct llist *myList;
	//List creation
	myList = llist_create(23);

	//List Addition
	llist_add(&myList, 37);
	llist_add(&myList, 41);
	printf("List addition\n");
	llist_print(myList);

	//List Removal
	llist_remove(&myList);
	printf("\nList removal\n");
	llist_print(myList);

	//Sum of Elements
	printf("\nSum: %f\n", llist_sum(myList));
	llist_destroy(myList);


	//Append 2 list
	struct llist *a = llist_create(10);
	llist_add(&a, 20);

	struct llist *b = llist_create(11);
	llist_add(&b, 21);

	ll_append(a, b);

	printf("\nList after append\n");
	llist_print(a);

	//....Cleanup
	llist_destroy(a);
	//llist_destroy(b); REDUNDANT. Destroying of 'a' cleaned 'b' as well.



	//Attempts at utilizing cmp
	printf("\nTest: %s\n", compare(2,2, &cmp) ? "True" : "False");

	struct llist *aa = llist_create(10);
	llist_add(&aa, 20);

	struct llist *bb = llist_create(11);
	llist_add(&bb, 20);


	//---ll_equal_by comparison
	printf("\nComparison of two lists (aa, bb)\n");
	printf("Lists match: %s\n", ll_equal_by(aa, bb, &cmp) ? "True" : "False");

	llist_destroy(aa);
	llist_destroy(bb);

	struct llist *duplicates = llist_create(60);
	llist_add(&duplicates, 10);
	llist_add(&duplicates, 20);
	llist_add(&duplicates, 20);
	llist_add(&duplicates, 30);
	llist_add(&duplicates, 40);
	llist_add(&duplicates, 50);
	llist_add(&duplicates, 10);

	printf("\nList before flattening\n");
	llist_print(duplicates);

	printf("\nList after flattening\n");
	ll_flatten(duplicates, &cmp);
	llist_print(duplicates);

	printf("\nIs sorted?\n");
	printf("%s\n", ll_is_sorted(duplicates, &cmp) ? "True" : "False");

	printf("\nInsert Sorted\n");
	ll_insert_sorted(&duplicates, 35);
	llist_print(duplicates);

	printf("\nReverse this list\n");
	ll_reverse(&duplicates);
	llist_print(duplicates);

	printf("\nIs Circular?\n");
	printf("%s\n", ll_is_circular(duplicates) ? "True" : "False");

	llist_destroy(duplicates);
}










