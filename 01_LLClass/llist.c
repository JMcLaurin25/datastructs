
#include "llist.h"
#include <stdio.h>
#include <stdlib.h>


struct llist *ll_create(void *data)
{
	struct llist *head = malloc(sizeof(*head));
	if(head) {
		head->data = data;
		head->next = NULL;
	}
	return head;
}

void ll_destroy(struct llist *list)
{
	while(list) {
		struct llist *tmp = list->next;
		free(list->data);
		free(list);
		list = tmp;
	}
}

void ll_disassemble(struct llist *list)
{
	while(list) {
		struct llist *tmp = list->next;
		free(list);
		list = tmp;
	}
}

void ll_add(struct llist **list, void *data)
{
	struct llist *item = ll_create(data);
	if(item) {
		item->next = *list;
		*list = item;
	}
}

void *ll_remove(struct llist **list)
{
	struct llist *old_head = *list;
	*list = old_head->next;
	old_head->next = NULL;
	void *data = old_head->data;
	free(old_head);
	return data;
}

void ll_append(struct llist *a, struct llist *b)
{
	while(a->next) {
		a = a->next;
	}

	a->next = b;
}

bool ll_is_circular(struct llist *list)
{
	struct llist *begin = list;
	while(list) {
		if (list->next == begin) {
			return true;
		}
		list = list->next;
	}
	return false;
}

void ll_reverse(struct llist **list)
{
	struct llist *curr_node = *list;
	struct llist *prev_node = NULL;
	struct llist *next_node;

	while(curr_node) {
		next_node = curr_node->next;
		curr_node->next = prev_node;
		prev_node = curr_node;
		curr_node = next_node;
	}
	*list = prev_node;
}

bool ll_is_sorted(struct llist *list, int (*cmp)(void *, void *))
{
	while (list && list->next) {
		if(1 == cmp(list->data, list->next->data)) {
			return false;
		}

		list = list->next;
	}

	return true;
}


