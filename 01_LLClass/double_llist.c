
#include "llist.h"
#include <stdio.h>
#include <stdlib.h>


struct llist *ll_create(double data)
{
	struct llist *head = malloc(sizeof(*head));
	if(head) {
		head->data = data;
		head->next = NULL;
	}
	return head;
}

void ll_destroy(struct llist *l)
{
	while(l) {
		struct llist *tmp = l->next;
		free(l);
		l = tmp;
	}
}

void ll_add(struct llist **l, double data)
{
	struct llist *item = ll_create(data);
	if(item) {
		item->next = *l;
		*l = item;
	}
}

void ll_remove(struct llist **l)
{
	struct llist *old_head = *l;
	*l = old_head->next;
	old_head->next = NULL;
	ll_destroy(old_head);
}

double ll_sum(struct llist *list)
{
	double total = 0;

	while (list) {
		total += list->data;
		list = list->next;
	}

	return total;
}

void ll_print(struct llist *list)
{
	while (list) {
		printf("%lf -> ", list->data);
		list = list->next;
	}
	printf("NULL\n");
}

void ll_append(struct llist *a, struct llist *b)
{
	while(a->next) {
		a = a->next;
	}

	a->next = b;
}

bool ll_is_circular(struct llist *list)
{
	struct llist *begin = list;
	while(list) {
		if (list->next == begin) {
			return true;
		}
		list = list->next;
	}
	return false;
}

void ll_reverse(struct llist **list)
{
	struct llist *curr_node = *list;
	struct llist *prev_node = NULL;
	struct llist *next_node;

	while(curr_node) {
		next_node = curr_node->next;
		curr_node->next = prev_node;
		prev_node = curr_node;
		curr_node = next_node;
	}
	*list = prev_node;
}

bool ll_is_sorted(struct llist *list, int (*cmp)(double, double))
{
	while (list && list->next) {
		if(1 == cmp(list->data, list->next->data)) {
			return false;
		}

		list = list->next;
	}

	return true;
}

void ll_insert_increasing(struct llist **list, double value)
{
	struct llist *cur_node = *list;
	struct llist *prev_node = NULL;
	struct llist *new_node = ll_create(value);

	while (cur_node && cur_node->data <= value) {
		prev_node = cur_node;
		cur_node = cur_node->next;
	}

	if (prev_node) {
		prev_node->next = new_node;
	} else {
		*list = new_node;
	}

	new_node->next = cur_node;
}






