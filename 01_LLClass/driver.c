
#include <stdio.h>
#include "llist.h"

int increasing(double a, double b)
{
	if (a < b) {
		return -1;
	} else if (a > b) {
		return 1;
	} else {
		return 0; //0 returns match
	} 
}

int decreasing(double a, double b)
{
	if (a > b) {
		return -1;
	} else if (a < b) {
		return 1;
	} else {
		return 0; //0 returns match
	} 
}


int main(void)
{
	struct llist *list = ll_create(5.3);
	ll_add(&list, 3.1);
	ll_add(&list, 4.2);
	ll_add(&list, 6.0);

	ll_print(list);

	printf("Sum: %lf\n", ll_sum(list));

	ll_add(&list, -8.8);
	ll_print(list);

	ll_destroy(list);

	struct llist *a = ll_create(1.9);
	ll_add(&a, 1.1);
	struct llist *b = ll_create(2.6);
	ll_add(&b, 2.2);

	ll_print(a);
	ll_print(b);

	ll_append(a, b);
	ll_print(a);

	ll_reverse(&a);
	ll_print(a);

	if (ll_is_sorted(a, increasing)) {
		printf("List sorted increasing\n");
	} else if (ll_is_sorted(a, decreasing)) {
		printf("List sorted decreasing\n");
	} else {
		printf("List not sorted\n");
	}

	ll_insert_increasing(&a, 97);
	ll_insert_increasing(&a, 0);
	ll_insert_increasing(&a, -90000);
	ll_print(a);

	ll_destroy(a);
}
