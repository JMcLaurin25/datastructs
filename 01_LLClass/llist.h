
#ifndef LLIST_H
#define LLIST_H

#include <stdbool.h>

struct llist {
	void *data;
	struct llist *next;
};

struct llist *ll_create(void *data);
void ll_destroy(struct llist *l);
void ll_disassemble(struct llist *list);

void ll_add(struct llist **l, void *data);
void *ll_remove(struct llist **l);

void ll_append(struct llist *a, struct llist *b);
bool ll_is_circular(struct llist *list);
void ll_reverse(struct llist **list);

bool ll_is_sorted(struct llist *list, int(*cmp)(void *, void *));

#endif
