
#ifndef TREE_H
#define TREE_H

#include <stdio.h>
#include <stdbool.h>

//Element Architecture
struct node {
	size_t value;
};

struct tree {
	struct node *data;
	struct tree *left, *right;
	int height;
};

typedef struct {
	struct tree *root;
	int (*cmp)(const struct node *, const struct node *);
} bst;

//Creation functions
struct node *node_create(char *symbol, char *name, double node);
bst *bst_create(int (*cmp)(const struct node *, const struct node *));

//Modification functions
bool bst_insert(bst *cur_bst, struct node *stock);

//Break-down stage
void node_destroy(struct node *);
void tree_destroy(struct tree *cur_tree);
void tree_disassemble(struct tree *cur_tree);
void bst_destroy(bst *cur_bst, void (*dismantle)(struct tree *cur_tree));

//Printing of data
void print_cur_tree_data(struct tree *t);
void print_cur_bst_data(bst *cur_bst);

//node comparator functions
int cmp_num_increase(const struct node *, const struct node *);

#endif
