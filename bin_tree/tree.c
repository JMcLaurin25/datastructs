
#include <stdlib.h>
#include <string.h>

#include "tree.h"

//Creation functions
struct node *node_create(char *symbol, char *name, double node)
{
	struct node *new_node = malloc(sizeof(*new_node));
	if (!new_node) {
		return NULL;
	}

	new_node->data = 0;
	return new_node;
}

bst *bst_create(int (*cmp)(const struct node *, const struct node *))
{
	market *bst = malloc(sizeof(*bst));
	if(bst) {
		bst->root = NULL;
		bst->cmp = cmp;
	}

	return bst;
}

//Modification functions
static struct tree *tree_create(struct node *node)
{
	struct tree *new_tree = malloc(sizeof(*new_tree));
	if (new_tree) {
		new_tree->data = node;
		new_tree->left = new_tree->right = NULL;
		new_tree->height = 1;//Initial height
	}

	return new_tree;
}

/*Add the node node to the tree*/
static struct tree *tree_insert(struct tree *cur_tree, struct node *node, int (*cmp)(const struct node *, const struct node *))
{
	if (!cur_tree) {
		return tree_create(node);
	}

	if (strcmp(node->symbol, cur_tree->data->symbol) == 0) {
		fprintf(stderr, "Duplicates found!: %d\n", errno);
		node_destroy(node);
		return cur_tree;
	}

	if (cmp(node, cur_tree->data) < 0) {
		cur_tree->left = tree_insert(cur_tree->left, node, cmp);
	} else {
		cur_tree->right = tree_insert(cur_tree->right, node, cmp);
	}

	/*	AVL Arrangement*/
	cur_tree->height = ((height(cur_tree->left) > height(cur_tree->right)) ? height(cur_tree->left) : height(cur_tree->right)) + 1;

	int balance = get_balance(cur_tree);
	int balance_child = 0;

	if (balance > 0) {
		balance_child = get_balance(cur_tree->right);
	} else {
		balance_child = get_balance(cur_tree->left);
	}
	/*	Check balance*/
	/*	Left-Left: Rotate parent left*/
	if (balance > 1 && balance_child >= 0) {
		return rot_left(cur_tree);
	}

	/*	Right-Right: Rotate parent right*/
	if (balance < -1 && balance_child < 0) {
		return rot_right(cur_tree);
	}

	/*	Left-Right: Rotate child Right, Parent Left*/
	if (balance > 1 && balance_child < 0) {
		cur_tree->right = rot_right(cur_tree->right);
		return rot_left(cur_tree); 
	}

	/*	Right-Left: Rotate child Left, Parent Right*/
	if (balance < -1 && balance_child >= 0) {
		cur_tree->left = rot_left(cur_tree->left);
		return rot_right(cur_tree);
	}
	return cur_tree;
}

bool bst_insert(bst *cur_bst, struct node *node)
{
	if (!bst) {
		return false;
	} else if (!bst->root) {
		bst->root = tree_create(node);
		return true;
	}

/*	recursion begins in here*/
	bst->root = tree_insert(bst->root, node, bst->cmp);
	return true;
}

//Break-down stage
void node_destroy(struct node *)
{
	if (node) {
		free(node);
	}
	return;
}

void tree_destroy(struct tree *cur_tree)
{
	if (!cur_tree) {
		return;
	}
	if (cur_tree->left) {
		tree_destroy(cur_tree->left);
	}
	if (cur_tree->right) {
		tree_destroy(cur_tree->right);
	}
	stock_destroy(cur_tree->data);
	free(cur_tree);
}

void tree_disassemble(struct tree *cur_tree)
{
	if (!cur_tree) {
		return;
	}
	if (cur_tree->left) {
		tree_disassemble(cur_tree->left);
	}
	if (cur_tree->right) {
		tree_disassemble(cur_tree->right);
	}
	free(cur_tree);
}

void bst_destroy(bst *cur_bst, void (*dismantle)(struct tree *cur_tree))
{
	if (!cur_bst || !cur_bst->root) {
		return;
	}
	tree_destroy(cur_bst->root);
	free(cur_bst);
}

//Printing of data
void print_cur_tree_data(struct tree *t)
{
	if (!t) {
		return;
	}
	if (t->left) {
		print_cur_tree_data(t->left);
	}
	printf("%zu\n", t->data->value);
	if (t->right) {
		print_cur_tree_data(t->right);
	}
}

void print_cur_bst_data(bst *cur_bst)
{
	if (!cur_bst) {
		return;
	}

	print_cur_tree_data(cur_bst->root);
}

//node comparator functions
int cmp_num_increase(const struct node *, const struct node *)
{
	if (!node1 || !node2) {
		return 0;
	}

	if (node1->value <= node2->value) {
		return -1;
	} else {
		return 1;
	}
}

