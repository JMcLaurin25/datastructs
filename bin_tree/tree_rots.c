
#include "tree_rots.h"



int height(struct tree *t)
{
    if (!t) {
        return 0;
	}
    return t->height;
}

int get_balance(struct tree *t)
{
    if (!t) {
        return 0;
	}
    return height(t->right) - height(t->left);
}

/*Rotate to the right*/
struct tree *rot_right(struct tree *t)
{
	if (!t) {
		return NULL;
	}

    struct tree *old_left = t->left;
    struct tree *tmp = old_left->right;

/*	Rotate pointers*/
    old_left->right = t;
    t->left = tmp;

/*	Height update*/
	t->height = (height(t->left) > height(t->right) ? height(t->left) : height(t->right)) + 1;
	old_left->height = (height(old_left->left) > height(old_left->right) ? height(old_left->left) : height(old_left->right)) + 1;

/*	Reset the root*/
    return old_left;
}

/*Rotate to the left*/
struct tree *rot_left(struct tree *t)
{
 	if (!t) {
		return NULL;
	}

	struct tree *old_right = t->right;
    struct tree *tmp = old_right->left;
 
/*	Rotates pointers*/
    old_right->left = t;
    t->right = tmp;
 
/*	Height update*/
	t->height = (height(t->left) > height(t->right) ? height(t->left) : height(t->right)) + 1;
	old_right->height = (height(old_right->left) > height(old_right->right) ? height(old_right->left) : height(old_right->right)) + 1;
 
/*	Reset the root*/
    return old_right;
}



