
#ifndef TREE_ROTS_H
#define TREE_ROTS_H


int height(struct tree *t);
int get_balance(struct tree *t);

struct tree *rot_right(struct tree *t);	//Rotate to the right
struct tree *rot_left(struct tree *t);	//Rotate to the left

#endif
