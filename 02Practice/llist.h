#ifndef LLIST_H
#define LLIST_H

struct llist {
	double data;
	struct llist *head;
	struct llist *tail;
};

struct llist *ll_create(double data);
void ll_destroy(struct llist *list);

void ll_add(struct llist **list, double data);
void ll_remove(struct llist **list, double data);

void ll_print(struct llist *list);
double ll_sum(struct llist *a);

#endif
