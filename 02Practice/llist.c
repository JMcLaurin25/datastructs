#include "llist.h"
#include <stdio.h>
#include <stdlib.h>

struct llist *ll_create(double data)
{
	struct llist *new_node = malloc(sizeof(new_node));
	if (new_node) {
		new_node->data = data;
		new_node->head = NULL;
		new_node->tail = NULL;		
	}
	return new_node;
}

void ll_destroy(struct llist *list)
{
	while(list) {
		struct llist *temp = list->tail;
		free(list);
		list = temp;
	}
}

void ll_add(struct llist **list, double data)
{
	struct llist *new_node = ll_create(data);

	if(new_node) {
		new_node->tail = *list;
		(*list)->head = new_node;
		*list = new_node;
	}
}

void ll_print(struct llist *list)
{
	while (list) {
		printf("%lf -> ", list->data);
		list = list->tail;
	}
	printf("NULL\n");
}

double ll_sum(struct llist *list)
{
	double total = 0;
	while (list) {
		total += list->data;
		list = list->next;
	}

	return total;
}











