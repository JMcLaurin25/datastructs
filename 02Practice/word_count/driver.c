
#include <stdio.h>
#include "hash.h"

int main(void)
{
	FILE *fp;
	char *input_buf = malloc(256);

	fp = fopen("test.txt", "r");
	if (!fp) {
		return 1;
	}

	hash *hash_words = hash_create();
	if (!hash_words) {
		fclose(fp);
		return 1;
	}

	while (!feof(fp)) {
		if(!fgets(input_buf, 256, fp)) {
			continue;
		}
		hash_insert(hash_words, input_buf, hash_fetch(hash_words, input_buf) + 1);
	}

	for (size_t n=0; n < hash_words->capacity; ++n) {
		//struct h_llist *tmp = hash_words->data[n];
		while (hash_words->data[n]) {
			printf("Count: (%d): %s", (int)hash_words->data[n]->value, hash_words->data[n]->key);
			hash_words->data[n] = hash_words->data[n]->next;
		}
	}

}

